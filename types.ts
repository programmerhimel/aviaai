interface Lead {
  _id: string;
  avatar: string;
  name: string;
  role: string;
  url: string;
  country: string;
  category: string;
  company: string;
  location: string;
  industry: string;
  note: string;
  tags: string[];
  resource: string;
  meta_data: Record<string, any>;
  assigned_user: string;
  assigned_account: string;
  action_status: string;
  accept_status: string;
  created_at: string;
  updated_at: string;
}

interface Account {
  _id: string;
  name: string;
  user: string;
  status: "active" | "pending" | "restricted" | "archived" | "assigned";
  created_at: string;
  updated_at: string;
}

interface User {
  _id: string;
  name: string;
  email: string;
  password: string;
  role: "user" | "admin";
  avatar: string;
  created_at: string;
  updated_at: string;
}
