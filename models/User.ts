import bcrypt from 'bcrypt';
import { Schema, Document, model, CallbackError } from 'mongoose';

// Define the User interface
interface User extends Document {
  name: string;
  email: string;
  password: string;
  role: 'user' | 'admin';
  avatar: string;
  created_at: Date;
  updated_at: Date;
  isModified: (path: string) => boolean; // Add this method to match the mongoose document interface
}

// Define the User schema
const UserSchema = new Schema<User>({
  name: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  role: { type: String, enum: ['user', 'admin'], default: 'user' },
  avatar: { type: String },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});

// Pre-save hook to hash the password
UserSchema.pre<User>('save', async function (next) {
  const user = this;
  if (!user.isModified('password')) return next();

  try {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(user.password, salt);
    user.password = hash;
    next();
  } catch (error) {
    next(error as CallbackError); // Type assertion here
  }
});

const UserModel = model<User>('User', UserSchema);

export default UserModel;
