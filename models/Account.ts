import mongoose, { Document, Model } from "mongoose";
import { AccountStatusEnum } from "./enumModels";

export interface Account extends Document {
  name: string;
  user: string;
  status: AccountStatusEnum;
  createdAt: Date;
  updatedAt: Date;
}

const AccountSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    user: { type: String, required: true },
    status: {
      type: String,
      enum: Object.values(AccountStatusEnum),
      required: true,
    },
  },
  { timestamps: true }
);

const AccountModel: Model<Account> =
  mongoose.models.Account || mongoose.model<Account>("Account", AccountSchema);

export default AccountModel;
