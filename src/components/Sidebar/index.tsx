import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";

function Sidebar() {
  const router = useRouter();
  const path = router.pathname;
  const routes = [
    {
      name: "Users",
      path: "/users",
      icon: "fa-solid fa-users",
    },
    { name: "Accounts", path: "/accounts", icon: "fa-solid fa-landmark" },
    { name: "Leads", path: "/leads", icon: "fa-solid fa-address-card" },
  ];
  return (
    <div className="h-full text-white w-full max-w-64 px-5 border-r border-gray-800">
      <div className="flex flex-col justify-between items-start">
        {routes.map((route, index) => (
          <Link
            className={`flex items-center gap-3 w-full px-3 py-2 ${
              path === route.path ? "bg-[#181F37]" : ""
            } hover:bg-[#181F37] rounded my-2`}
            href={route.path}
            key={index}
          >
            <i className={`${route.icon}`}></i>
            <p>{route.name}</p>
          </Link>
        ))}
      </div>
    </div>
  );
}

export default Sidebar;
