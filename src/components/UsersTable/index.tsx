import React, { useEffect, useMemo, useRef, useState } from "react";
import type { SyntheticListenerMap } from "@dnd-kit/core/dist/hooks/utilities";
import { useSortable } from "@dnd-kit/sortable";
import { CSS } from "@dnd-kit/utilities";
import { Table, Avatar } from "antd";
import type { TableColumnsType, TableProps } from "antd";
import { usersData } from "@/data";

type TableRowSelection<T> = TableProps<T>["rowSelection"];

interface RowContextProps {
  setActivatorNodeRef?: (element: HTMLElement | null) => void;
  listeners?: SyntheticListenerMap;
}

const RowContext = React.createContext<RowContextProps>({});

const columns: TableColumnsType<User> = [
  {
    title: "Avatar",
    dataIndex: "avatar",
    width: 75,
    render: (avatar: string) => <Avatar src={avatar} />,
  },
  { title: "Name", dataIndex: "name" },
  { title: "Email", dataIndex: "email" },
  { title: "Role", dataIndex: "role" },
  {
    title: "Created At",
    dataIndex: "created_at",
    render: (date: string) => new Date(date).toLocaleString(),
  },
  {
    title: "Updated At",
    dataIndex: "updated_at",
    render: (date: string) => new Date(date).toLocaleString(),
  },
];

interface RowProps extends React.HTMLAttributes<HTMLTableRowElement> {
  "data-row-key": string;
}

const Row: React.FC<RowProps> = (props) => {
  const {
    attributes,
    listeners,
    setNodeRef,
    setActivatorNodeRef,
    transform,
    transition,
    isDragging,
  } = useSortable({ id: props["data-row-key"] });

  const style: React.CSSProperties = {
    ...props.style,
    transform: CSS.Translate.toString(transform),
    transition,
    ...(isDragging ? { position: "relative", zIndex: 9999 } : {}),
  };

  const contextValue = useMemo<RowContextProps>(
    () => ({ setActivatorNodeRef, listeners }),
    [setActivatorNodeRef, listeners]
  );

  return (
    <RowContext.Provider value={contextValue}>
      <tr {...props} ref={setNodeRef} style={style} {...attributes} />
    </RowContext.Provider>
  );
};

function UsersTable() {
  const [dataSource, setDataSource] = useState<User[]>(usersData);
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const elementRef = useRef<HTMLDivElement>(null);
  const [elementHeight, setElementHeight] = useState(0);

  useEffect(() => {
    const updateHeight = () => {
      if (elementRef.current) {
        const height = elementRef.current.getBoundingClientRect().height;
        setElementHeight(height);
      }
    };

    const resizeObserver = new ResizeObserver(() => {
      updateHeight();
    });

    if (elementRef.current) {
      resizeObserver.observe(elementRef.current);
    }

    updateHeight();

    return () => {
      if (elementRef.current) {
        resizeObserver.unobserve(elementRef.current);
      }
    };
  }, []);

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log("selectedRowKeys changed: ", newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection: TableRowSelection<User> = {
    selectedRowKeys,
    onChange: onSelectChange,
    // selections: [
    //   Table.SELECTION_ALL,
    //   Table.SELECTION_INVERT,
    //   Table.SELECTION_NONE,
    //   {
    //     key: "odd",
    //     text: "Select Odd Row",
    //     onSelect: (changeableRowKeys) => {
    //       let newSelectedRowKeys = [];
    //       newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
    //         if (index % 2 !== 0) {
    //           return false;
    //         }
    //         return true;
    //       });
    //       setSelectedRowKeys(newSelectedRowKeys);
    //     },
    //   },
    //   {
    //     key: "even",
    //     text: "Select Even Row",
    //     onSelect: (changeableRowKeys) => {
    //       let newSelectedRowKeys = [];
    //       newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
    //         if (index % 2 !== 0) {
    //           return true;
    //         }
    //         return false;
    //       });
    //       setSelectedRowKeys(newSelectedRowKeys);
    //     },
    //   },
    // ],
  };

  return (
    <div ref={elementRef} className="flex-1 h-full overflow-hidden">
      <Table
        bordered
        rowKey="_id"
        components={{ body: { row: Row } }}
        rowSelection={rowSelection}
        columns={columns}
        dataSource={dataSource}
        scroll={{ y: elementHeight ? elementHeight - 130 : 0 }}
        className="shadow-md rounded-lg"
        sticky
      />
    </div>
  );
}

export default UsersTable;
