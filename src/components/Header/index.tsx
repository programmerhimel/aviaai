import { Dropdown, MenuProps } from "antd";
import { signOut, useSession } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";

function Header() {
  const { data: session } = useSession();
  const router = useRouter();

  if (!session) {
    return (
      <p className="mx-5 my-10 px-3 py-2 text-white rounded-md bg-gray-500">
        You are not logged in.{" "}
        <Link className="text-red-700" href={"/signin"}>
          Signin
        </Link>
      </p>
    );
  }

  if (!session.user) {
    return (
      <p className="mx-5 my-10 px-3 py-2 text-white rounded-md bg-gray-500">
        User information not available.
      </p>
    );
  }

  const { user } = session;

  const handleLogout = async () => {
    await signOut();
    router.push("/signin");
  };
  const items: MenuProps["items"] = [
    {
      label: <button onClick={handleLogout}>Logout</button>,
      key: "0",
    },
  ];
  return (
    <div className="w-full text-white text-sm font-medium">
      <div className="flex justify-end items-center px-5 py-5">
        <div className="flex items-center bg-[#172037] gap-2 px-2 py-1 rounded ml-1">
          <i className="fa-solid fa-magnifying-glass text-xs bg-inherit"></i>
          <input
            type="search"
            name="search"
            id="header-search"
            placeholder="Search"
            className="bg-inherit focus:outline-none"
          />
        </div>
        <div className="flex items-center gap-2 font-normal group">
          <i className="fa-solid fa-arrow-up-right-from-square text-xs bg-inherit"></i>
          <Dropdown menu={{ items }}>
            <div className="flex gap-2">
              <div className="relative w-6 h-6 rounded-full overflow-hidden ml-2 group-hover:scale-110 transition">
                <Image src={"/images/profile.jpg"} alt="Profile" fill />
              </div>
              <h3>{user.email}</h3>
            </div>
          </Dropdown>
        </div>
      </div>
    </div>
  );
}

export default Header;
