import React from "react";
import Sidebar from "../Sidebar";
import Header from "../Header";

function Layout({ children }: any) {
  return (
    <div className="flex flex-col">
      <Header />
      <div className="main flex w-full h-auto">
        <Sidebar />
        <div className="flex flex-col w-[calc(100%-256px)] h-full">
          <div className="content bg-white w-full h-full overflow-y-auto">
            {children}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Layout;
