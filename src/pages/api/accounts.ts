import { NextApiRequest, NextApiResponse } from "next";
import dbConnect from "../../../lib/db";
import AccountModel, { Account } from "../../../models/Account";

dbConnect();

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { method } = req;

  switch (method) {
    case "GET":
      try {
        const accounts = await AccountModel.find({});
        res.status(200).json(accounts);
      } catch (error) {
        console.error("Error fetching accounts:", error);
        res.status(500).json({ error: "Error fetching accounts" });
      }
      break;

    case "POST":
      try {
        const accountData: Account = req.body;
        const newAccount = await AccountModel.create(accountData);
        res.status(201).json(newAccount);
      } catch (error) {
        console.error("Error creating account:", error);
        res.status(500).json({ error: "Error creating account" });
      }
      break;

    case "PUT":
      try {
        const { id } = req.query;
        const accountData: Partial<Account> = req.body;

        const updatedAccount = await AccountModel.findByIdAndUpdate(
          id,
          accountData,
          {
            new: true,
            runValidators: true,
          }
        );

        if (!updatedAccount) {
          res.status(404).json({ error: "Account not found" });
        } else {
          res.status(200).json(updatedAccount);
        }
      } catch (error) {
        console.error("Error updating account:", error);
        res.status(500).json({ error: "Error updating account" });
      }
      break;

    case "DELETE":
      try {
        const { id } = req.query;
        const deletedAccount = await AccountModel.findByIdAndDelete(id);
        if (!deletedAccount) {
          res.status(404).json({ error: "Account not found" });
        } else {
          res.status(200).json({ message: "Account deleted successfully" });
        }
      } catch (error) {
        console.error("Error deleting account:", error);
        res.status(500).json({ error: "Error deleting account" });
      }
      break;

    default:
      res.setHeader("Allow", ["GET", "POST", "PUT", "DELETE"]);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}
