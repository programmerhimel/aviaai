import { NextApiRequest, NextApiResponse } from "next";
import dbConnect from "../../../lib/db";
import LeadModel, { Lead } from "../../../models/Lead";

dbConnect();

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { method } = req;

  switch (method) {
    case "GET":
      try {
        const leads = await LeadModel.find({});
        res.status(200).json(leads);
      } catch (error) {
        console.error("Error fetching leads:", error);
        res.status(500).json({ error: "Error fetching leads" });
      }
      break;

    case "POST":
      try {
        const leadData: Lead = req.body;
        const newLead = await LeadModel.create(leadData);
        res.status(201).json(newLead);
      } catch (error) {
        console.error("Error creating lead:", error);
        res.status(500).json({ error: "Error creating lead" });
      }
      break;

    case "PUT":
      try {
        const { id } = req.query;
        const leadData: Partial<Lead> = req.body;
        const updatedLead = await LeadModel.findByIdAndUpdate(id, leadData, {
          new: true,
          runValidators: true,
        });
        if (!updatedLead) {
          res.status(404).json({ error: "Lead not found" });
        } else {
          res.status(200).json(updatedLead);
        }
      } catch (error) {
        console.error("Error updating lead:", error);
        res.status(500).json({ error: "Error updating lead" });
      }
      break;

    case "DELETE":
      try {
        const { id } = req.query;
        const deletedLead = await LeadModel.findByIdAndDelete(id);
        if (!deletedLead) {
          res.status(404).json({ error: "Lead not found" });
        } else {
          res.status(200).json({ message: "Lead deleted successfully" });
        }
      } catch (error) {
        console.error("Error deleting lead:", error);
        res.status(500).json({ error: "Error deleting lead" });
      }
      break;

    default:
      res.setHeader("Allow", ["GET", "POST", "PUT", "DELETE"]);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}
