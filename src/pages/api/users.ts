import { NextApiRequest, NextApiResponse } from "next";
import bcrypt from "bcrypt";
import dbConnect from "../../../lib/db";
import UserModel from "../../../models/User";

dbConnect();

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { method } = req;

  switch (method) {
    case "GET":
      try {
        const users = await UserModel.find({});
        res.status(200).json(users);
      } catch (error) {
        console.error("Error fetching users:", error);
        res.status(500).json({ error: "Error fetching users" });
      }
      break;

    case "POST":
      try {
        const userData: User = req.body;
        const newUser = await UserModel.create(userData);
        res.status(201).json(newUser);
      } catch (error) {
        console.error("Error creating user:", error);
        res.status(500).json({ error: "Error creating user" });
      }
      break;

    case "PUT":
      try {
        const { id } = req.query;
        const userData: Partial<User> = req.body;

        if (userData.password) {
          const salt = await bcrypt.genSalt(10);
          userData.password = await bcrypt.hash(userData.password, salt);
        }

        const updatedUser = await UserModel.findByIdAndUpdate(id, userData, {
          new: true,
          runValidators: true,
        });

        if (!updatedUser) {
          res.status(404).json({ error: "User not found" });
        } else {
          res.status(200).json(updatedUser);
        }
      } catch (error) {
        console.error("Error updating user:", error);
        res.status(500).json({ error: "Error updating user" });
      }
      break;

    case "DELETE":
      try {
        const { id } = req.query;
        const deletedUser = await UserModel.findByIdAndDelete(id);
        if (!deletedUser) {
          res.status(404).json({ error: "User not found" });
        } else {
          res.status(200).json({ message: "User deleted successfully" });
        }
      } catch (error) {
        console.error("Error deleting user:", error);
        res.status(500).json({ error: "Error deleting user" });
      }
      break;

    default:
      res.setHeader("Allow", ["GET", "POST"]);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}
