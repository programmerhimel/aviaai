import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect } from "react";

function Home() {
  const router = useRouter();
  useEffect(() => {
    router.push("/users");
  }, []);

  return (
    <div>
      <Head>
        <title>Avia.ai | Event Management Application</title>
      </Head>
      <div className="w-full h-screen p-5 overflow-auto">
        <h2 className="">Content</h2>
      </div>
    </div>
  );
}

export default Home;
