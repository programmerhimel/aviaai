import LeadTable from "@/components/LeadTable";
import { Dropdown, MenuProps, Space } from "antd";
import Head from "next/head";
import React from "react";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";

function Leads() {
  const items: MenuProps["items"] = [
    {
      label: "All",
      key: "0",
    },
    {
      label: "User",
      key: "1",
    },
    {
      label: "Admin",
      key: "2",
    },
  ];
  return (
    <div className="flex flex-col w-full h-[calc(100vh-68px)] overflow-hidden relative">
      <Head>
        <title>Leads | Event Management Application</title>
      </Head>
      <div className="p-5 my-5 mx-5 border rounded-md shadow-sm sticky top-0 z-50 bg-white">
        <div className="flex justify-between items-center">
          <div className="flex gap-3 items-center">
            <h1 className="text-lg font-bold text-gray-700">Leads</h1>
          </div>
          <div className="flex gap-3 items-center w-full justify-end">
            <div className="flex items-center bg-[#F9F9F5] gap-2 px-3 py-2 rounded-md ml-1 w-full max-w-[350px]">
              <i className="fa-solid fa-magnifying-glass text-gray-500 text-sm bg-inherit"></i>
              <input
                type="search"
                name="search"
                id="header-search"
                placeholder="Search"
                className="bg-inherit focus:outline-none text-lg w-full"
              />
            </div>
            <button className="flex gap-2 items-center bg-[#F9D300] hover:bg-[#c7af26] rounded-md px-3 py-2 text-base font-semibold text-gray-700">
              <i className="fa-solid fa-plus"></i>
              <span>Add User</span>
            </button>
            <button className="flex gap-2 items-center bg-[#F9F9F5] hover:bg-gray-400 rounded-md p-3 text-base font-semibold text-gray-700">
              <i className="fa-solid fa-ellipsis"></i>
            </button>
          </div>
        </div>
        <div className="mt-5 flex justify-between items-center gap-3">
          <h2 className="font-semibold text-gray-600">Showing 1 to 10 of 20</h2>
          <div className="flex gap-3 items-center">
            <Dropdown menu={{ items }} trigger={["click"]}>
              <a
                className="border rounded-md px-3 py-2"
                onClick={(e) => e.preventDefault()}
              >
                <Space>
                  Role
                  <ArrowDropDownIcon />
                </Space>
              </a>
            </Dropdown>
            <Dropdown menu={{ items }} trigger={["click"]}>
              <a
                className="border rounded-md px-3 py-2"
                onClick={(e) => e.preventDefault()}
              >
                <Space>
                  Status
                  <ArrowDropDownIcon />
                </Space>
              </a>
            </Dropdown>
            <Dropdown menu={{ items }} trigger={["click"]}>
              <a
                className="border rounded-md px-3 py-2"
                onClick={(e) => e.preventDefault()}
              >
                <Space>
                  Created
                  <ArrowDropDownIcon />
                </Space>
              </a>
            </Dropdown>
            <button className="flex gap-2 items-center bg-white hover:bg-[#F9F9F5] rounded-md px-3 py-2 text-base font-semibold text-gray-700 border shadow">
              <span>Add Filter</span>
              <i className="fa-solid fa-plus"></i>
            </button>
          </div>
        </div>
      </div>
      <div className="px-5 w-full h-full">
        <LeadTable />
      </div>
    </div>
  );
}

export default Leads;
